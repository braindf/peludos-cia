'use strict';

const {Client} = require('pg');
var DataHelper = require('./DataHelper');
var config = require('../config/config');
var conString = config.database;

module.exports = function PostManager() {
    return {
        buscarPosts: function(quantidade,offset, callback) {
            //console.log("OFFSET = "  + offset);
            const client = new Client ({
                connectionString : conString
            });
            client.connect(function(err) {
                if (err) {
                    //console.log(err);
                }
                var query = "SELECT post.id, titulo, conteudo, usuario.nome AS autor, post.data_atualizacao, data_publicacao FROM post " +
                             "INNER JOIN usuario ON autor = usuario.id " +
                             "ORDER BY data_publicacao DESC " +
                             "LIMIT " + quantidade + " OFFSET " + offset + ";";

                //console.log(query);
                client.query(query,function(err,result) {
                    if(err) {
                        client.end();
                        //console.error('error running query', err);
                        callback(null);
                    }
                    else {
                        client.end();
                        if (result.rowCount > 0) {
                            var dataHelper = new DataHelper();
                            for (var i = 0; i < result.rowCount; i++) {
                                result.rows[i].dataFormatada = dataHelper.formataDataDoBancoDeDados(result.rows[i].data_publicacao);
                            }
                            callback(result.rows);
                        }
                        else {
                            if (result.rowCount == 0) {
                                var model = {
                                    vazio: true
                                };
                                callback(model);
                            }
                            else {
                                callback(null);
                            }
                        }
                    }
                });
            });
        },
        buscarPost: function(id,callback) {
            const client = new Client ({
                connectionString : conString
            });
            client.connect(function(err) {
                if (err) {
                    //console.log(err);
                }
                var query = "SELECT  post.id, titulo, conteudo, usuario.nome AS autor, post.data_atualizacao, data_publicacao FROM post " +
                            "INNER JOIN usuario on autor = usuario.id " +
                            "WHERE post.id =" + id + ";";


                client.query(query,function(err,result) {
                    if(err) {
                        client.end();
                        //console.error('error running query', err);
                        callback(null);
                    }
                    else {
                        client.end();
                        if (result.rowCount > 0) {
                            var ret = result.rows[0];
                            var dataHelper = new DataHelper();
                            ret.dataFormatada = dataHelper.formataDataDoBancoDeDados(ret.data_publicacao);
                            callback(ret);
                        }
                        else {
                            if (result.rowCount == 0) {
                                var ret = {
                                    vazio: true
                                };
                                callback(ret);
                            }
                            else {
                                callback(null);
                            }
                        }
                    }
                });
            });
        },
        getQuantidadePosts : function(callback) {
            const client = new Client ({
                connectionString : conString
            });
            client.connect(function(err) {
                if (err) {
                    //console.log(err);
                    callback(null);
                }
                var query = "SELECT count(*) as quantidade FROM post";

                //console.log('query = ' + query);
                client.query(query,function(err,result) {
                    var model;
                    if(err) {
                        client.end();
                        //console.error('error running query', err);
                        model = null;
                    }
                    else {
                        model = result.rows[0].quantidade
                    }
                    callback(model);
                });
            });
        },
        inserePost: function (post, callback) {
            const client = new Client ({
                connectionString : conString
            });
            client.connect(function(err) {
                if (err) {
                    //console.log(err);
                    callback(null);
                }
                var query = "INSERT INTO post(titulo, conteudo, autor, data_atualizacao, data_publicacao) VALUES " +
                            "('" + post.titulo +"','" + post.conteudo +"'," + post.autor + ",current_timestamp, current_timestamp) RETURNING id;";

                //console.log('query = ' + query);
                client.query(query,function(err,result) {
                    var model;
                    if(err) {
                        client.end();
                        //console.error('error running query', err);
                        model = {
                            sucesso: false
                        };
                    }
                    else {
                        //console.log(result);
                        model = {
                            sucesso: true,
                            id : result.rows[0].id
                        };
                    }
                    callback(model);
                });
            });
        },
        removePost: function (id, callback) {
            const client = new Client ({
                connectionString : conString
            });
            client.connect(function(err) {
                if (err) {
                    //console.log(err);
                    callback(null);
                }
                var query = "DELETE FROM post WHERE id=" + id + ";";
                //console.log('query = ' + query);
                client.query(query,function(err,result) {
                    var model;
                    if(err) {
                        client.end();
                        //console.error('error running query', err);
                        model = {
                            sucesso: false
                        };
                    }
                    else {
                        model = {
                            sucesso: true
                        };
                    }
                    callback(model);
                });
            });
        },
        atualizarTextoTituloPost: function(id,texto,titulo,callback) {
            const client = new Client ({
                connectionString : conString
            });
            client.connect(function(err) {
                if (err) {
                    //console.log(err);
                    callback(null);
                }
                var query = "UPDATE post SET titulo='" + titulo + "', conteudo='" + texto + "' WHERE id=" + id + ";" ;

                //console.log('query = ' + query);
                client.query(query,function(err,result) {
                    var model;
                    if(err) {
                        client.end();
                        //console.error('error running query', err);
                        model = {
                            sucesso: false
                        };
                    }
                    else {
                        model = {
                            sucesso: true
                        };
                    }
                    callback(model);
                });
            });
        }
    };
};
