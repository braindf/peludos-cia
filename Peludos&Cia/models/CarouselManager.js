'use strict';

const {Client} = require('pg');
var ClientPgNative = require('pg-native')
var DataHelper = require('./DataHelper');
var config = require('../config/config');
var conString = config.database;

module.exports = function CarouselManager() {
    return {
        adicionarCarousel: function(imagem,legenda, link) {
            /*const client = new Client ({
                connectionString : conString
            });
            client.connect(function(err) {
                if (err) {
                    console.log(err);
                }
                var params = [image,legenda,link];
                var query = "INSERT INTO carousel(imagem, legenda, link) VALUES " +
                            "($1,$2,$3) RETURNING id;";

                //console.log(query);
                //var ret = await client.query(query,params);
                console.log(ret);
                callback(ret.rows[0].id);
            });*/
            //console.log('entrou aqui');
            try {
                var client = new ClientPgNative();
                client.connectSync(conString);
                var params = [imagem,legenda,link];
                //console.log(params);
                var query = "INSERT INTO carousel(imagem, legenda, link) VALUES " +
                            "($1,$2,$3) RETURNING id;";
                //console.log(query);
                var rows = client.querySync(query,params);
                //console.log('ID = '  + rows[0].id);
                return rows[0].id;
            }
            catch (err) {
                //console.log(err);
                return -1;
            }
        },
        buscarCarousels: function(callback) {
            const client = new Client ({
                connectionString : conString
            });
            client.connect(function(err) {
                if (err) {
                    //console.log(err);
                }
                var query = "SELECT  * FROM carousel;" ;


                client.query(query,function(err,result) {
                    if(err) {
                        client.end();
                        //console.error('error running query', err);
                        callback(null);
                    }
                    else {
                        client.end();
                        if (result.rowCount > 0) {
                            var ret = result.rows;
                            var dataHelper = new DataHelper();
                            callback(ret);
                        }
                        else {
                            if (result.rowCount == 0) {
                                var ret = {
                                    vazio: true
                                };
                                callback(ret);
                            }
                            else {
                                callback(null);
                            }
                        }
                    }
                });
            });
        },
        removeCarousels: function (ids, callback) {
            const client = new Client ({
                connectionString : conString
            });
            client.connect(function(err) {
                if (err) {
                    //console.log(err);
                    callback(null);
                }

                var query = "DELETE FROM carousel WHERE id in (" + ids + ");";
                //console.log('query = ' + query);
                client.query(query,function(err,result) {
                    var model;
                    if(err) {
                        client.end();
                        //console.error('error running query', err);
                        model = {
                            sucesso: false
                        };
                    }
                    else {
                        model = {
                            sucesso: true
                        };
                    }
                    callback(model);
                });
            });
        }
    };
};
