'use strict';

var PostManager = require('../../models/PostManager');


module.exports = function (router) {

    const postManager = new PostManager();

    router.get(/^\/\d+$/, function (req, res) {
        var id = req.url.substring(1);
        //console.log(req.url);
        //console.log(id);
        //console.log(Number.isInteger(parseInt(id)));
        if (Number.isInteger(parseInt(id))) { // Is a number
            postManager.buscarPost(id,function(post) {
                //console.log(post);
                if (post.vazio) {
                    res.redirect('/pages/blog');
                }
                else {
                    if (req.session.logado) {
                        post.podeModificar = true;
                        post.id = id;
                    }
                    //console.log(post);
                    post.session = req.session;
                    res.render('post', post);
                }

            });
        }
        else {
            res.redirect('/pages/blog');
        }
    });

    router.get('/adicionarPost', function (req, res) {
        if (req.headers.type != 'https') {
            res.redirect('/pages/blog');
            return;
        }

        if (req.session.logado) {
            var  model = {};
            model.session = req.session;
            res.render('adicionarPost', model);
        }
        else {
            res.redirect('/');
        }
    });

    router.post('/adicionarPost', function (req, res) {
        if (req.headers.type != 'https') {
            res.redirect('/pages/blog');
            return;
        }

        if (req.session.logado) {
            var titulo = req.body.titulo;
            var conteudo = req.body.conteudo;
            var autor = req.session.login_id;
            var post = {
                titulo : titulo,
                conteudo: conteudo,
                autor:autor
            };
            postManager.inserePost(post,function(retorno) {
                res.redirect('/posts/' + retorno.id);
            });
        }
        else {
            res.redirect('/');
        }
    });

    router.post('/removerPost', function (req, res) {
        if (req.headers.type != 'https') {
            res.redirect('/pages/blog');
            return;
        }

        if (req.session.logado) {
            var postId = req.body.id;
            //console.log(req.body);
            postManager.removePost(postId,function(retorno) {
                //console.log("RETORNO = " + retorno);
                req.session.deletedPost = req.body.titulo;
                res.redirect('/pages/blog');
            });
        }
        else {
            res.redirect('/');
        }
    });

    router.get(/^\/editarPost\/\d+$/, function(req,res){
        if (req.headers.type != 'https') {
            res.redirect('/pages/blog');
            return;
        }

        if (req.session.logado) {
            var id = req.url.substring(12);
            //console.log(id);
            //console.log(Number.isInteger(parseInt(id)));
            if (Number.isInteger(parseInt(id))) { // Is a number
                postManager.buscarPost(id,function(retorno) {
                    //console.log(retorno);
                    retorno.session = req.session;
                    res.render('editarPost',retorno);
                });
            }
            else {
                res.redirect('/');
            }
        }
        else {
            res.redirect('/');
        }
    });

    router.post('/atualizarTextoTituloPost', function(req,res){
        if (req.headers.type != 'https') {
            res.redirect('/pages/blog');
            return;
        }

        if (req.session.logado) {

            postManager.atualizarTextoTituloPost(req.body.id,req.body.conteudo,req.body.titulo,function(sucesso) {
                //console.log(retorno);
                if (sucesso) {
                    res.redirect('/posts/' + req.body.id);
                }
                else {
                    var model = {
                        erroAoAtualizar : true
                    };
                    res.redirect('/editarPost/' + req.body.id);
                }
            });
        }
        else {
            res.redirect('/');
        }
    });

};
