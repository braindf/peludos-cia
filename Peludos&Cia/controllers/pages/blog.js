'use strict';

var PostManager = require('../../models/PostManager');
const quantidadePadrao = 5;


module.exports = function (router) {

    const postManager = new PostManager();

    router.get('/', function (req, res) {
        var offset,quantidade;
        if ((req.query.start == undefined) || (req.query.quantidade == undefined)) {
            offset = 0;
            quantidade = quantidadePadrao;
        }
        else {
            offset = req.query.start*1;
            quantidade = req.query.quantidade*1;
        }

        if (offset < 0) {
            offset = 0;
        }
        if (quantidade <= 0) {
            quantidade = quantidadePadrao;
        }

        //console.log(offset);
        //console.log(quantidade);
        postManager.getQuantidadePosts(function(quantPosts){
            //console.log(quantPosts);
            if (offset >= quantPosts) {
                offset = 0;
            }
            postManager.buscarPosts(quantidade,offset,function(posts) {
                    //console.log(posts);
                    for (var i = 0; i < posts.length; i++) {
                        var aux  = posts[i].conteudo;
                        //console.log("aux = " + aux);
                        var fim = aux.search("</p>");
                        //console.log("fim = " + fim);
                        aux = aux.substring(0,fim + 4);
                        //console.log(aux);
                        fim = aux.search(">");
                        aux = aux.substring(fim+1,aux.length-4);
                        //console.log(aux);
                        posts[i].resumo = aux;
                    }

                    var model = {
                        posts: posts
                    }

                    //calculo dos offsets para as barras embaixo
                    if (offset + quantidade < quantPosts) {
                        model.temPostsAntigos = true;
                        model.antigo_offset = offset + quantidade;
                        model.antigo_quantidade = quantidadePadrao;
                    }
                    else {
                        model.semPostAntigos = true;
                    }



                    if (offset  > 0) {
                        model.temPostsRecentes = true;
                        model.recente_offset = offset - quantidade;
                        model.recente_quantidade = quantidadePadrao;
                    }
                    else {
                            model.semPostRecentes = true;
                    }

                    //se um usuario logado pode adicionar posts
                    if (req.session.logado) {
                        model.podeAdicionar = true;
                    }
                    //se acabou de deletar
                    if (req.session.deletedPost != undefined) {
                        model.postDeletado = req.session.deletedPost;
                        delete req.session.deletedPost;
                    }

                    //console.log(model);
                    model.session = req.session;

                    res.render('blog', model);
            });
        });
    });

};
