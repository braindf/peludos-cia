'use strict';

var PostManager = require('../../models/PostManager');
var LoginManager = require('../../models/LoginManager');

module.exports = function (router) {

    const loginManager = new LoginManager();

    router.get('/', function (req, res) {
        //console.log(req.headers);
        if ((req.headers.type == 'https') && (!req.session.logado)) {
            res.render('login');
        }
        else {
            res.redirect('/');
        }

    });

    router.get('/logout', function (req, res) {
        //console.log(req.headers);
        if ((req.headers.type == 'https') && (req.session.logado)) {
            delete req.session.logado;
            delete req.session.logado;
            delete req.session.nome;
            delete req.session.login;
            delete req.session.login_id;
        }
        res.redirect('/');

    });

    router.post('/login', function (req, res) {
        //console.log("Entrou: Login Post");
        //console.log(req.body.login);
        //console.log(req.body.password);
        //loginManager.gerarHashSenha(req.body.login, req.body.password);
        if ((req.headers.type != 'https')  || (req.session.logado)) {
            res.redirect('/');
            return;
        }

        loginManager.verificaLogin(req.body.login, req.body.password, function(usuario) {
            if (usuario != null) {
                var session = req.session;
                session.logado = true;
                session.nome = usuario.nome;
                session.login = req.body.login;
                session.login_id = usuario.login_id;
                session.justLogged = true;

                //console.log(session);
                res.redirect('/');
            }
            else  {
                var model = {
                    msgRetorno : true
                };
                res.render('login',model);
            }
        });

    });

    /*router.get('/cadastrar', function (req, res) {
        //console.log(req.headers);
        if (req.headers.type == 'https') {
            res.render('cadastro');
        }
        else {
            res.redirect('/');
        }

    });*/

    router.post('/cadastrar', function (req, res) {
        //console.log("Entrou: Login Post");
        //console.log(req.body.login);
        //console.log(req.body.password);
        //loginManager.gerarHashSenha(req.body.login, req.body.password);
        if (req.headers.type != 'https') {
            res.redirect('/');
            return;
        }

        loginManager.gerarHashSenha(req.body.login, req.body.password, function(retorno) {
            if(retorno) {
                res.render('login');
            }
            else {
                res.render('error/404');
            }
        });

    });
};
