'use strict';

var PostManager = require('../models/PostManager');
var CarouselManager = require('../models/CarouselManager');

module.exports = function (router) {

    const postManager = new PostManager();
    const carouselManager = new CarouselManager();

    router.get('/', function (req, res) {

        var offset = 0;
        var quantidade = 3;
        postManager.buscarPosts(quantidade,offset,function(posts) {
                //console.log(posts);

                for (var i = 0; i < posts.length; i++) {
                    var aux  = posts[i].conteudo;
                    var fim = aux.search("</p>");
                    aux = aux.substring(0,fim + 4);
                    fim = aux.search(">");
                    aux = aux.substring(fim+1,aux.length-4);
                    posts[i].resumo = aux;
                }

                var model = {
                    posts: posts
                }

                model.session = req.session;

                if (req.session.justLogged) {
                    model.showMsg = true;
                    delete model.session.justLogged;
                }
                if (req.session.logado) {
                    model.editarCarousel = true;
                }

                carouselManager.buscarCarousels(function(carousels){
                    //console.log(carousels);
                    if (carousels.length > 0) {
                        model.temCarousel = true;
                    }
                    for (var i = 0; i < carousels.length; i++) {
                        carousels[i].index = i;
                    }
                    model.carousels = carousels;
                    res.render('index', model);
                });
        });
    });

};
