'use strict';

const {Client} = require('pg');
const bcrypt = require('bcrypt');
var DataHelper = require('./DataHelper');
var config = require('../config/config');
var conString = config.database;
const saltRounds = 10;

module.exports = function LoginManager() {
    return {
        verificaLogin: function(login,password,callback) {
            const client = new Client ({
                connectionString : conString
            });
            client.connect(function(err) {
                if (err) {
                    //console.log(err);
                }
                var query =  "SELECT * FROM usuario WHERE login='" + login + "'";

                //console.log(query);
                client.query(query,function(err,result) {
                    if(err) {
                        client.end();
                        //console.error('error running query', err);
                        callback(null);
                    }
                    else {
                        client.end();
                        //console.log(result.rows);
                        if (result.rowCount > 0) {
                            var hash_senha = result.rows[0].hash_senha;
                            //console.log(hash_senha);
                            if (bcrypt.compareSync(password,hash_senha)) {
                                var ret = {
                                    nome: result.rows[0].nome,
                                    login: result.rows[0].login,
                                    login_id: result.rows[0].id
                                };
                                callback(ret);
                            }
                            else {
                                callback(null);
                            }
                        }
                        else {
                            callback(null);
                        }
                    }
                });
            });
        },
        gerarHashSenha: function(login,senha,func) {
            bcrypt.hash(senha, saltRounds, function(err, hash) {
                if (err) {
                    //console.log("Erro bcrypt: "  + err);
                }
                // Store hash in your password DB.
                const client = new Client ({
                    connectionString : conString
                });
                client.connect(function(err) {
                    if (err) {
                        //console.log(err);
                    }
                    var query =  "UPDATE usuario SET hash_senha='" + hash +"' WHERE login='" + login +"'";
                    //console.log(query);
                    client.query(query,function(err,result) {
                        if (err) {
                            //console.log("Erro ao inserir senha: "  + err);
                            func(false);
                        }
                        else {
                            //console.log(result);
                            if (result.rowCount == 1) {
                                func(true);
                            }
                            else {
                                func(false);
                            }
                        }
                    });
                });
            });
        }
    }
}
