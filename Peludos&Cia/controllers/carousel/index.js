'use strict';

var CarouselManager = require('../../models/CarouselManager');

module.exports = function (router) {

    const carouselManager = new CarouselManager();

    router.get('/gerenciar', function (req, res) {
        if (req.headers.type != 'https') {
            res.redirect('/');
            return;
        }

        if (req.session.logado) {
            carouselManager.buscarCarousels(function(carousels){
                //console.log(carousels);
                var model = {};
                if (carousels.length > 0) {
                    model.temCarousel = true;
                    for (var i = 0; i < carousels.length; i++) {
                        carousels[i].index = i;
                    }

                    model.carousels = carousels;
                }
                else {
                    model.temCarousel = false;
                }

                model.session = req.session;

                //console.log(model);
                res.render('gerenciarCarousel',model);
            });
        }
        else {
            res.redirect('/');
        }
    });

    router.post('/inserirAtualizar', function (req, res) {
        if (req.headers.type != 'https') {
            res.redirect('/');
            return;
        }

        if (req.session.logado) {
            //console.log(req.body.carousel);
            var items = req.body.carousel;
            var deleted = req.body.imgDeletadas;

            for (var i = 0; items && i < items.length; i++) {
                //console.log('Item: ' + i);
                if (!items[i].id) {
                    var id = carouselManager.adicionarCarousel(items[i].imagem,items[i].legenda,items[i].link);
                    //console.log(id);
                }
                else {
                    //console.log('Não é novo!');
                }
            }
            if (deleted) {
                //console.log(deleted);
                carouselManager.removeCarousels(deleted, function(retorno){
                    res.redirect('/carousel/gerenciar');
                });
            }
            else {
                res.redirect('/carousel/gerenciar');
            }
        }
        else {
            res.redirect('/');
        }
    });

};
