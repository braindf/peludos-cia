'use strict';

var IndexModel = require('../../models/index');


module.exports = function (router) {

    var model = new IndexModel();

    router.get('/', function (req, res) {

        model.session = req.session;
        res.render('contato', model);


    });

};
